let expect  = require("chai").expect;
let chaiHttp = require('chai-http');
let server = require('../app');
let request = require('request');


// Main  block to run the test case
describe('Players Stats', () => {
	beforeEach((done) => {		
	   console.log("Player state has been started");	   
	   done();
	});

	afterEach(function () {
	    console.log("Player state has been fended");
	});

	it('responds to /', (done) => {
	  let server = "http://localhost:3000"; // ?pid=35320	
	  request(server, (error,response,body) => {
	  	expect(response.statusCode).to.equal(200);
	  	done();
	  });	    
	});

	// Test the get method to get the player details usig pid
	describe('/get player', () => {
		it('It should GET player details', (done) => {
			let url = "http://localhost:3000/player?pid=35320"; // ?pid=35320			
			request(url, (error,response,body) => {
				let responseData = JSON.parse(body);
				expect(responseData[0].msg).to.equal(true);
				expect(response.statusCode).to.equal(200);
        		expect(Object.keys(responseData[0]).length).to.not.equal(0);
        		expect(responseData[0].hasOwnProperty('error')).to.equal(false);
        		done();
			});					
		})
	})
});