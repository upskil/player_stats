import request from 'request';
import Configuration from './Configuration';

class Stats extends Configuration {

	constructor () {
		super();
		this.responseData = [];		
	}

	callApi(pid) {		
		return new Promise((resolved, rejected) => {
			request(`${this.apiUrl}&pid=${pid}`, (error, response, body) => {
				if (error)  {
					this.responseData.push({
						"msg" : false,
						"data" : JSON.parse(error) 
					});
					rejected(error);
				}
				if (body) {
					this.responseData.push({
						"msg" : true,
						"data" : JSON.parse(body) 
					});
					resolved(this.responseData);

				} 
			}); 
		});		
	}
}

export default Stats;