import express from 'express';
import stats from './classess/stats';
const router = express.Router();

/* GET index page. */
router.get('/', (req, res, next) => {
  	let playerId = req.query.pid;
  	console.log(playerId);
  	let data = new stats().callApi(playerId)
  	.then(result => res.status('200').json(result)) 
  	.catch(error => res.status('400').json(error));  	
});
//JSON.parse(JSON.parse(json).data));
export default router;
