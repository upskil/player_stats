angular.module('interview-base')
	.controller('PlayerController', ['$scope','PlayerService', function ($scope,PlayerService) {
		
		$scope.message = "";		
		$scope.sidebar = {};
		$scope.sidebar.show = false;
		// Save the todo list
		$scope.searchPlayer = function () {
			const todoData = {name: $scope.playerName}
			PlayerService.searchPlayer($scope.playerName)
			.then( (res) => {
				// $scope.message = res.data.msg;
				
				let playerStats = angular.fromJson(res);				
				console.log('-------------');
				console.log(playerStats.data[0].data);
				console.log('-------------');
				$scope.sidebar.show = true;
				$scope.playerStats = playerStats.data[0].data;
				
			})
			.catch(error => {
				console.log(error);
				var errorResponse = angular.fromJson(error);
				$scope.message = errorResponse[0];
			});
		}

		//$scope.test = $scope.statusOption[0];
		

  }]);
