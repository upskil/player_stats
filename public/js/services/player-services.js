angular.module('interview-base')
  .service('PlayerService', playerService);  

  function playerService($http) {
  	this.searchPlayer = searchPlayer;
  	

  	function searchPlayer(pid) {
  		return $http.get(`/player?pid=${pid}`);  		
  	}  	
  }
