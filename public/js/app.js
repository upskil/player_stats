// Declare app level module which depends on filters, and services
angular.module('interview-base', ['ngResource', 'ngRoute', 'ui.bootstrap', 'ui.date'])
  .config(['$routeProvider','$locationProvider', function ($routeProvider,$locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/home/home.html', 
        controller: 'HomeController'})

      .when('/player', {
			templateUrl: 'views/player/list.html',
			controller: 'PlayerController'})

      .otherwise({redirectTo: '/'});
    $locationProvider.hashPrefix('');
  }]);
